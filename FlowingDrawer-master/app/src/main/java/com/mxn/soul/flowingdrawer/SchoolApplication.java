package com.mxn.soul.flowingdrawer;

import android.app.Application;

public class SchoolApplication extends Application {

    public static SchoolApplication INSTANCE;

    @Override
    public void onCreate(){
        super.onCreate();
        INSTANCE = this;
    }

    public static SchoolApplication getINSTANCE() {
        return INSTANCE;
    }
}
