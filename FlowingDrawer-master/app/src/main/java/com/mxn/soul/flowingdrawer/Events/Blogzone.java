package com.mxn.soul.flowingdrawer.Events;

/**
 * Created by EKENE on 11/4/2017.
 */

public class Blogzone {

    private String title, desc, imageUrl, username, date;



    public Blogzone(String title, String desc, String imageUrl, String username, String date) {
        this.title = title;
        this.desc = desc;
        this.imageUrl=imageUrl;
        this.username = username;
        this.date = date;

    }

    public Blogzone() {
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {

        return date;
    }

}
