package com.mxn.soul.flowingdrawer.list;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mxn.soul.flowingdrawer.AllUsersActivity;
import com.mxn.soul.flowingdrawer.MainActivity;
import com.mxn.soul.flowingdrawer.R;

import java.util.ArrayList;

public class TeachersFragment extends Fragment {
    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    public DatabaseReference mUsersDatabase;

    private ActionMode mActionMode;

    private ProgressDialog mProgressDialog;

    private DatabaseReference mFriendReqDatabase;
    private DatabaseReference mUpdateDatabase;
    private DatabaseReference mRef;
    private DatabaseReference mDatabase;

    private DatabaseReference mRootRef;

    SharedPreferences sharedPreferences;

    private FirebaseUser mCurrent_user;
    MainActivity mainActivity;

    private String mCurrent_state;

    private TextView counter_text;
    FirebaseRecyclerAdapter<Users,AllUsersActivity.UserViewHolder> firebaseRecyclerAdapter;


    public TeachersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teachers, container, false);

        mUsersList = (RecyclerView) view.findViewById(R.id.users_list);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        mRootRef = FirebaseDatabase.getInstance().getReference();

        mUsersDatabase.keepSynced(true);

        final String user_id = getActivity().getIntent().getStringExtra("user_id");

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Teachers");
        mFriendReqDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mRef = FirebaseDatabase.getInstance().getReference("Users").child("Teachers");//.child("Users");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mCurrent_state = "not_friends";

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        counter_text = (TextView) view.findViewById(R.id.counterText);

        mProgressDialog = new ProgressDialog(getContext());

        mUsersList.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {


                if (mActionMode != null) {
                    return false;
                }

                mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);
                return true;
            }
        });


        return view;
    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(final ActionMode mode, final Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_multi_select, menu);
            //            mode.setTitle("Choose your option");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }


        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = current_user.getUid();

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_role:

                    counter = 0;
                    CharSequence options[] = new CharSequence[]{"Present", "Absent"};

                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    builder.setTitle("Select");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            //Click Event for each item.
                            if (i == 0) {

                                for(int j=0;j<selectedUsers.size();j++) {

                                    uid=selectedUsers.get(j).getId();


                                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Attendance").child("03-10-18").child("garvit");
                                    mProgressDialog.setTitle("Updating...");
                                    mProgressDialog.setMessage("Please wait while we update!");
                                    mProgressDialog.setCanceledOnTouchOutside(false);
                                    mProgressDialog.show();
                                    mDatabase.child("Present").setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {

                                                mProgressDialog.dismiss();
                                                mActionMode.finish();
                                                selectedUsers.clear();
                                                mUsersList.setAdapter(firebaseRecyclerAdapter);
                                                counter= 0;

                                            }

                                        }
                                    });

                                }
                            }

                            if (i == 1) {

                                for (int j = 0; j < selectedUsers.size(); j++) {

                                    uid = selectedUsers.get(j).getId();

                                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                                    mProgressDialog.setTitle("Updating...");
                                    mProgressDialog.setMessage("Please wait while we update!");
                                    mProgressDialog.setCanceledOnTouchOutside(false);
                                    mProgressDialog.show();
                                    mDatabase.child("Absent").setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {

                                                mProgressDialog.dismiss();
                                                Toast.makeText(getContext(), "Now a Developer as well", Toast.LENGTH_SHORT).show();

                                            }

                                        }
                                    });

                                }
                            }

                        }
                    });

                    builder.show();
                    counter = 0;
                    mode.finish();

                    return true;

                case R.id.action_remove_role:

                    counter = 0;
                    CharSequence options_remove[] = new CharSequence[]{"Present", "Absent"};

                    final AlertDialog.Builder builder_remove = new AlertDialog.Builder(getContext());

                    builder_remove.setTitle("Select to Change");
                    builder_remove.setItems(options_remove, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

//                            sharedPreferences = getContext().getSharedPreferences("NEW_USER_ID",getContext().MODE_PRIVATE);
//                            PreferenceManager.getDefaultSharedPreferences(getContext()).getString("USER_ID", uid);
                            //Click Event for each item.
                            if (i == 0) {

                                mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("USER_ID", uid));
                                mProgressDialog.setTitle("Updating...");
                                mProgressDialog.setMessage("Please wait while we update!");
                                mProgressDialog.setCanceledOnTouchOutside(false);
                                mProgressDialog.show();
                                mDatabase.child("Present").setValue("false").addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful()){

                                            mProgressDialog.dismiss();
                                            Toast.makeText(getContext(), "No longer a manager", Toast.LENGTH_SHORT).show();

                                        }

                                    }
                                });


                            }

                            if (i == 1) {
                                String key = mRef.push().getKey();

                                mUpdateDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(key);
                                mProgressDialog.setTitle("Updating...");
                                mProgressDialog.setMessage("Please wait while we update!");
                                mProgressDialog.setCanceledOnTouchOutside(false);
                                mProgressDialog.show();
                                mUpdateDatabase.child("Absent").setValue("false").addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful()){

                                            mProgressDialog.dismiss();
                                            Toast.makeText(getContext(), "No longer a Developer", Toast.LENGTH_SHORT).show();

                                        }

                                    }
                                });

                            }

                        }
                    });

                    builder_remove.show();
                    counter = 0;
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            counter = 0;
        }
    };


    private ArrayList<Users> selectedUsers = new ArrayList<>();

    int counter = 0;
    private boolean longPressed = false;


    private void select(Users users, AllUsersActivity.UserViewHolder viewHolder){
        if(selectedUsers.contains(users)) {
            viewHolder.mView.findViewById(R.id.ll_listitem).setBackgroundColor(getResources().getColor(R.color.white));
            selectedUsers.remove(users);
            counter=counter-1;
            counter(counter);
        } else {
            viewHolder.mView.findViewById(R.id.ll_listitem).setBackgroundColor(getResources().getColor(R.color.black_overlay));
            selectedUsers.add(users);
            counter=counter+1;
            counter(counter);
        }
        if(selectedUsers.size() == 0){
            longPressed = false;
        } else {
            longPressed = true;
        }

    }

    public void counter(int counter){

        if(counter == 0){
            mActionMode.finish();
        }
        else
        {
            if(counter==0){
                mActionMode.finish();
            }
            mActionMode.setTitle(selectedUsers.size()+" items selected");
        }

    }


    @Override
    public void onStart() {
        super.onStart();


        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, AllUsersActivity.UserViewHolder>(
                Users.class,
                R.layout.users_single_layout,
                AllUsersActivity.UserViewHolder.class,
                mUsersDatabase
        ) {
            @Override
            protected void populateViewHolder(final AllUsersActivity.UserViewHolder viewHolder, final Users users, final int position) {

                viewHolder.setDisplayName(users.getName());
                viewHolder.setUserStatus(users.getStatus());
                viewHolder.setUserImage(users.getImage(), getActivity());
                Log.d("VALUE",getRef(position).getKey());
                users.setId(getRef(position).getKey());
                viewHolder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
//                        Toast.makeText(mainActivity, "ID:"+getRef(position).getKey(), Toast.LENGTH_SHORT).show();
                        // mRef.child(mCurrent_user.getUid()).child("Roles1").child("Admin").setValue("TRUE");
                        mRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                 mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);
                                    select(users, viewHolder);
                                    final String other_user_id = getRef(position).getKey();
                                    PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("USER_ID", other_user_id).apply();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


//                        mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);
//                        select(users, viewHolder);
//                        prepareSelection(users,getView(),position);
                        return true;
                    }


                });

                final String user_id = getRef(position).getKey();


//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("USER_ID",user_id);
//                editor.apply();



                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(longPressed){
                            select(users, viewHolder);
                        } else {
                            CharSequence options[] = new CharSequence[]{"Open Profile", "Send message"};

                            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            builder.setTitle("Select Options");
                            builder.setItems(options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    //Click Event for each item.
                                    if (i == 0) {
//
//                                        Intent profileIntent = new Intent(getContext(), ProfileActivity.class);
//                                        profileIntent.putExtra("user_id", user_id);
//                                        startActivity(profileIntent);

                                    }

                                    if (i == 1) {
//
//                                        Intent chatIntent = new Intent(getContext(), ChatActivity.class);
//                                        chatIntent.putExtra("user_id", user_id);
//                                        chatIntent.putExtra("user_name", users.getName());
//                                        chatIntent.putExtra("user_image", users.getImage().toString());
//                                        startActivity(chatIntent);

                                    }

                                }
                            });

                            builder.show();
                        }

                    }
                });


            }
        };
        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }


    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    mActionMode.finish();
                    return true;
                }
                return false;
            }
        });
    }
}