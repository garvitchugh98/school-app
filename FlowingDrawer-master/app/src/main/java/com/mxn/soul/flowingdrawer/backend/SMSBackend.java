package com.mxn.soul.flowingdrawer.backend;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SMSBackend extends AsyncTask<String,String,String> {
    String msg;
    String num;
    public SMSBackend(String message, String numbers){
        msg = message;
        num = numbers;
    }
    @Override
    protected String doInBackground(String... strings) {
        String apiKey = "apikey=" + "x0EEF2YhBLc-1VbAspVEmVEYJUkkGaPorpSvqYKp0z";
        String message = "&message=" + msg;
        String numbers = "&numbers=" +num;
        String test = "&test=true"; //True set karne se message actually mai send nahi hoyega, credit kam nahi hoyega
        String s=null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
            String data = apiKey + numbers + message + test;
            Log.d("Data", "Data: " + data); //To check what data is being passed to the server
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");  //GET method se bhi kar sakta hai, but recommended nahi hai (security and performance issues)
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));   //Actual call statement which sends data to server
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            s = stringBuffer.toString();
            Log.d("StringBuffer","sendSMS: " + s);
            JSONObject response = new JSONObject(s);
//            String status = response.getJSONArray("errors").getString(0);
            String status = response.getString("status");
            Log.d("response", status);
            //Response server se. convert it to JSON using JSON parser aur "status" se check karlio ki send hua ki nahi
        }catch (Exception e){
            e.printStackTrace();
        }
        return s;
    }
}
