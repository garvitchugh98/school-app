package com.mxn.soul.flowingdrawer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mxn.soul.flowingdrawer.Events.EventActivity;
import com.mxn.soul.flowingdrawer.HolidaysAndEvents.AddActivity;
import com.mxn.soul.flowingdrawer.HolidaysAndEvents.ListDataActivity;
import com.mxn.soul.flowingdrawer.list.ListActivity;
import com.mxn.soul.flowingdrawer.notice.NoticeActivity;
import com.mxn.soul.flowingdrawer.notice.ViewUploadsActivity;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

//    private RecyclerView rvFeed;
    private FlowingDrawer mDrawer;
    GridLayout mainGrid;
    private FirebaseAuth mAuth;
    private DatabaseReference mref;
    private FirebaseUser mCurrent_user;
    private TextView signup;
    SharedPreferences sharedPreferences;
    String current_state = "TEACHER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        rvFeed = (RecyclerView) findViewById(R.id.rvFeed);
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);

//        rvFeed = (RecyclerView) findViewById(R.id.rvFeed);
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            mref = FirebaseDatabase.getInstance().getReference("Users");
            mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
//        Toast.makeText(this, ""+mCurrent_user.getUid(), Toast.LENGTH_SHORT).show();

            mref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child(mCurrent_user.getUid()).child("userType").getValue().equals("teacher")){

                        current_state = "Teacher";
                    }
                    else if(dataSnapshot.child(mCurrent_user.getUid()).child("userType").getValue().equals("student"))
                    {
                        current_state = "Student";
                        Toast.makeText(MainActivity.this, ""+current_state, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        setupToolbar();
//        setupFeed();

        signup = (TextView) findViewById(R.id.noAccount);
        mAuth = FirebaseAuth.getInstance(FirebaseApp.initializeApp(this));
        setupMenu();
        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        //Set Event
        setSingleEvent(mainGrid);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

    }

    protected void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
    }


    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (finalI){
                        case 0:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                            Intent Accountintent = new Intent(MainActivity.this,AccountSettingsActivity.class);
                            Accountintent.putExtra("info","This is activity from card item index  "+finalI);
                            startActivity(Accountintent);
                            break;

                        case 1:

                            if(current_state.equals("Teacher")){

                                Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                                Intent notice = new Intent(MainActivity.this,NoticeActivity.class);
                                notice.putExtra("info","This is activity from card item index  "+finalI);
                                startActivity(notice);

                            }else
                            if (current_state.equals("Student")){

                                Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                                Intent notice = new Intent(MainActivity.this,ViewUploadsActivity.class);
                                notice.putExtra("info","This is activity from card item index  "+finalI);
                                startActivity(notice);
                            }

                            break;

                        case 2:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                            Intent eventintent = new Intent(MainActivity.this,EventActivity.class);
                            eventintent.putExtra("info","This is activity from card item index  "+finalI);
                            startActivity(eventintent);
                            break;

                        case 3:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                            if(current_state.equals("Teacher")){

                                Intent intent = new Intent(MainActivity.this,AddActivity.class);
                                intent.putExtra("info","This is activity from card item index  "+finalI);
                                startActivity(intent);

                            }else
                                if (current_state.equals("Student")){
                                    Intent intent = new Intent(MainActivity.this,ListDataActivity.class);
                                    Toast.makeText(MainActivity.this, "Student", Toast.LENGTH_SHORT).show();
                                    intent.putExtra("info","This is activity from card item index  "+finalI);
                                    startActivity(intent);
                                }
                            break;


                        case 4:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                            Intent intent2 = new Intent(MainActivity.this,ListActivity.class);
                            intent2.putExtra("info","This is activity from card item index  "+finalI);
                            startActivity(intent2);
                            break;

                        case 5:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                            Intent calendar = new Intent(MainActivity.this,smsActivity.class);
                            calendar.putExtra("info","This is activity from card item index  "+finalI);
                            startActivity(calendar);
                            break;

                        default:
                            Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();


                    }


                }
            });
        }
    }


    private void setupMenu() {
        FragmentManager fm = getSupportFragmentManager();
        MenuListFragment mMenuFragment = (MenuListFragment) fm.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

//        current_state = sharedPreferences.getString("state","NOT_LOGGED_IN");
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){

            sendToStart();

       }
// else
//            if (current_state.equalsIgnoreCase("STUDENT")){
//
//
//                Intent startIntent = new Intent(MainActivity.this, ListActivity.class);
//                startActivity(startIntent);
//                finish();
//
//            }


    }


    @Override
    protected void onStop() {
        super.onStop();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null) {

//            mUserRef.child("online").setValue(ServerValue.TIMESTAMP);

        }

    }

    private void sendToStart() {

        Intent startIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(startIntent);
        finish();

    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
