package com.mxn.soul.flowingdrawer.list;

public class Users {

    private String Name;
    private String Image;
    private String Status;
    private String Thumb_Image;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Users(){


    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getThumb_Image() {
        return Thumb_Image;
    }

    public void setThumb_Image(String thumb_Image) {
        Thumb_Image = thumb_Image;
    }

    public Users(String name, String image, String status, String thumb_Image) {

        Name = name;
        Image = image;
        Status = status;
        Thumb_Image = thumb_Image;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Users(String name, String status) {

        Name = name;
        Status = status;
    }



}
