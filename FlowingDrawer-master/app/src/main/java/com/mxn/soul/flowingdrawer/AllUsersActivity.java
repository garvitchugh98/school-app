package com.mxn.soul.flowingdrawer;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mxn.soul.flowingdrawer.list.Users;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class AllUsersActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

//    public CheckBox checkBox;

    public DatabaseReference mUsersDatabase;

    public LinearLayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);

        mToolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUsersList = (RecyclerView) findViewById(R.id.users_list);
        mUsersList.setHasFixedSize(true);
        RecyclerView.LayoutManager LM = new LinearLayoutManager(this);
        mUsersList.setLayoutManager(LM);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

    }

    @Override
    protected void onStart() {
        super.onStart();

//        checkBox = (CheckBox) findViewById(R.id.checkBox);
//        checkBox.setVisibility(View.GONE);
//        checkBox.setChecked(false);

        FirebaseRecyclerAdapter<Users,UserViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UserViewHolder>(
                Users.class,
                R.layout.users_single_layout,
                UserViewHolder.class,
                mUsersDatabase
        ) {
            @Override
            protected void populateViewHolder(UserViewHolder viewHolder, Users users, int position) {

                viewHolder.setDisplayName(users.getName());
                viewHolder.setUserStatus(users.getStatus());
                viewHolder.setUserImage(users.getImage(), getApplicationContext());

                final String user_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        Intent profileIntent = new Intent(AllUsersActivity.this, ProfileActivity.class);
//                        profileIntent.putExtra("user_id", user_id);
//                        startActivity(profileIntent);

                    }
                });

//
//                viewHolder.mView.setOnLongClickListener(new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View view) {
//                        checkBox.setVisibility(View.VISIBLE);
//                        checkBox.setChecked(true);
//                        return true;
//                    }
//                });
            }
        };
        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

    public  static class UserViewHolder extends  RecyclerView.ViewHolder{

        public View mView;
        public UserViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String displayName) {
            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(displayName);
        }

        public void setUserStatus(String userStatus) {
            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(userStatus);

        }

        public void setUserImage(String image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(image).placeholder(R.drawable.default_avatar).into(userImageView);

        }
    }
}


